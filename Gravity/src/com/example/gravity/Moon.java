package com.example.gravity;

public class Moon extends Planet{

	float radius;
	float period;//milliseconds
	
	Moon(float mass1, float x1, float y1, float radius1, float period1) {
		super(mass1, x1, y1);
		radius=radius1;
		period=period1;
	}
	
	@Override
	float getX(int time){
		return x+radius*(float)Math.cos(time/period*2.0*Math.PI);
	}
	@Override
	float getY(int time){
		return y+radius*(float)Math.sin(time/period*2.0*Math.PI);
	}
}
