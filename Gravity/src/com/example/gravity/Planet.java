package com.example.gravity;

public class Planet {
	static final float sizeFactor=.3f;
	float mass;
	float x;
	float y;
	
	Planet(float mass1, float x1, float y1){
		mass=mass1;
		x=x1;
		y=y1;
	}
	
	float getMass(){
		return mass;
	}
	float getX(int time){
		return x;
	}
	float getY(int time){
		return y;
	}
	float getSize(){
		return mass*sizeFactor;
	}
	
}
