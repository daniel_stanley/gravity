package com.example.gravity;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

//import com.example.gravity.R;

import android.content.Context;
import android.opengl.GLES20;
//import android.opengl.Matrix;
import android.opengl.GLSurfaceView.Renderer;
import android.os.SystemClock;
import android.util.Log;

public class GravityRenderer implements Renderer{
	private Context MainActivityContext;
	private int shaderProgramHandle;
	private int[] mTextureDataHandles;
	private int mPositionHandle;
	private int mTexCoordinateHandle;
	private int mTextureUniformHandle;
	//private int mMVPMatrixHandle;
	//private float[] mMVPMatrix;
	private FloatBuffer mSquare;
	
	private float aspectRatio;
	private static Level2 level1;
	private static Rocket rocket1;
	private long lastTime=0;
	//private final double sizeRatio=.3;
	private static int time;

	public GravityRenderer (Context MAContext)
	{	
		MainActivityContext=MAContext;
		
		//Pack float buffers here if needed
	}


	@Override
	public void onSurfaceChanged(GL10 arg0, int width, int height) {
		aspectRatio=(float)height/width;
		Log.v("tag1","ratio: "+aspectRatio);
		Log.v("tag1","height: "+height);
		Log.v("tag1","width: "+width);
		// Set the OpenGL viewport to the same size as the surface.
		GLES20.glViewport(0, 0, width, height);
		//GLES20.glViewport(0, 0, 10, 10);
		
		
		/**
		// Create a new perspective projection matrix. The height will stay the same
		// while the width will vary as per aspect ratio.
		final float ratio = (float) height / width;
		final float left = -1;
		final float right = 1;
		final float bottom = 0f;
		final float top = 2.0f*ratio;
		final float near = 1.0f;
		final float far = 10.0f;


		Matrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
		**/
	}

	@Override
	public void onSurfaceCreated(GL10 arg0, EGLConfig arg1) {
		GLES20.glClearColor(0f, 0f, 0f, 0.0f);
		GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA,GLES20.GL_ONE_MINUS_SRC_ALPHA);
		
		final float[] squareArray={
				0f,1f,
				1f,1f,
				1f,0f,
				0f,0f};
		
		
		
	
		
		// Initialize the buffers.
		mSquare = ByteBuffer.allocateDirect(squareArray.length * 4)
        .order(ByteOrder.nativeOrder()).asFloatBuffer();
		mSquare.put(squareArray).position(0);
		
		
		
		
		final String vertexShaderTex=
			    	"attribute vec4 a_Position;     \n"		// Per-vertex position information we will pass in.
				  + "attribute vec2 a_TexCoordinate;        \n"		// Per-vertex texture coordinate information we will pass in. 					  
				  
				  + "varying vec2 v_TexCoordinate;          \n"		// This will be passed into the fragment shader.
				  
				  + "void main()                    \n"		// The entry point for our vertex shader.
				  + "{                              \n"
				  + "   v_TexCoordinate = a_TexCoordinate;          \n"		// Pass through the texture coordinate.
				  											// It will be interpolated across the triangle.
				  + "   gl_Position = a_Position;   \n"     // Multiply the vertex by the matrix to get the final point in 			                                            			 
				  + "}                              \n";    // normalized screen coordinates.
		
		
		final String fragmentShaderTex =
				"precision mediump float;       \n"		// Set the default precision to medium. We don't need as high of a 
														// precision in the fragment shader.				
			  + "uniform sampler2D u_Texture;   \n"     // The input texture.
			  + "varying vec2 v_TexCoordinate;          \n"		// This is the color from the vertex shader interpolated across the 
			  											// triangle per fragment.			  
			  + "void main()                    \n"		// The entry point for our fragment shader.
			  + "{                              \n"
			  + "   gl_FragColor = texture2D(u_Texture, v_TexCoordinate);     \n"		// Pass the color directly through the pipeline.		  
			  + "}                              \n";		
		
		final String[] attributes={"a_Position","a_TexCoordinate"};
		
		shaderProgramHandle=ShaderHelper.createShaders(vertexShaderTex,fragmentShaderTex, attributes);
		
		int[] textures= {R.drawable.ic_launcher,R.drawable.circle};
        mTextureDataHandles = ShaderHelper.loadTextures(MainActivityContext, textures);
		
        
       setLevel(2);
        
	}
	
	@Override
	public void onDrawFrame(GL10 arg0) {
		time+=10;
		Log.v("tag1", "time: "+time);
		int dt=(int) (SystemClock.uptimeMillis()-lastTime);
		lastTime+=dt;//usually I would do this at the end, but dt and the time will be changed by then
		//Log.v("Tag1","dt: "+dt);
		
		/**
		if(dt>50){
			dt=30;
			Log.v("Tag1","Clamping dt to 30");
		}
		**/
		
		
		GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);	
		rocket1.iterate(level1, .01, time, true);
		Rocket rocket2=rocket1.clone();
		
		outer:
		for(int i=0;i<100;i++){
			for(int j=0;j<10;j++){
				int crash=rocket2.iterate(level1, .01, time+10*(10*i+j+1), false);
				if(crash!=-1){
					break outer;
				}
			}
			drawSquare((float)(rocket2.x-.02)*aspectRatio,(float)rocket2.y-.02f,(float)(.04f*aspectRatio),.04f,1);
		}
		for(int i=0;i<level1.planets.length;i++){
			double diameter=level1.planets[i].getSize();
			drawSquare(
					(float)((level1.planets[i].getX(time)-diameter/2.0)*aspectRatio),
					(float)(level1.planets[i].getY(time)-diameter/2.0),
					(float)(diameter*aspectRatio),
					(float)(diameter),
					1);
		}
		
		double rocketSize=.1;
		drawSquare(
				(float)((rocket1.x-rocketSize/2.0)*aspectRatio),
				(float)(rocket1.y-rocketSize/2.0),
				(float)(rocketSize*aspectRatio),
				(float)(rocketSize),
				0);
		
		//Log.v("tag1","x:"+rocket1.x+"\ty:"+rocket1.y);
		if(GravityView.touching){
			drawSquare(GravityView.touchX-.1f,GravityView.touchY-.1f,.2f*aspectRatio,.2f,1);
		}
	}
	
	void drawSquare(float x, float y, float width, float height, int textureNumber){
		
		final float[] squareArray={
				x, y,
				x+width, y,
				x+width, y+height,
				x, y+height};
		
		FloatBuffer mSquare2 = ByteBuffer.allocateDirect(squareArray.length * 4)
        .order(ByteOrder.nativeOrder()).asFloatBuffer();
		mSquare2.put(squareArray).position(0);
		
		
		mPositionHandle = GLES20.glGetAttribLocation(shaderProgramHandle, "a_Position");
        mTexCoordinateHandle = GLES20.glGetAttribLocation(shaderProgramHandle, "a_TexCoordinate");
        
        mTextureUniformHandle = GLES20.glGetUniformLocation(shaderProgramHandle, "u_Texture");
        //mMVPMatrixHandle = GLES20.glGetUniformLocation(shaderProgramHandle, "u_MVPMatrix");
        
        GLES20.glUseProgram(shaderProgramHandle);
        // Set the active texture unit to texture unit 0.
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
        GLES20.glUniform1i(mTextureUniformHandle, 0);  
        
        
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandles[textureNumber]);
        
        
        
        
        mSquare.position(0);//mPositionOffset=0
        GLES20.glVertexAttribPointer(mPositionHandle, 2, GLES20.GL_FLOAT, false,
        		8, mSquare2);
        GLES20.glEnableVertexAttribArray(mPositionHandle);
        
        mSquare.position(0);//mPositionOffset=0
        GLES20.glVertexAttribPointer(mTexCoordinateHandle, 2, GLES20.GL_FLOAT, false,
        		8, mSquare);
        GLES20.glEnableVertexAttribArray(mTexCoordinateHandle); 
        
        
        
        
        
       
          
        
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, 4);
	}
	
	public static void setLevel(int num){
		time=0;
		level1=new Level2(num);
		rocket1=level1.getRocket();
		
	}

}
