package com.example.gravity;

//import android.util.Log;

public class Rocket {
	double x,y,vx,vy;
	//private static final double sizeRatio=.3;
	
	public Rocket(double x1, double y1){
		x=x1;
		y=y1;
	}
	public Rocket(double x1, double y1, double vx1, double vy1){
		x=x1;
		y=y1;
		vx=vx1;
		vy=vy1;
	}
	
	public Rocket clone(){
		Rocket clone=new Rocket(x,y,vx,vy);
		return clone;
	}
	
	//returns -1 for no crash, planet id otherwise
	public int iterate(Level2 level, double dt, int time, boolean useTouch){
		double accelx=0;
		double accely=0;
		int crashed=-1;
		
		//apply gravity
		int planets=level.planets.length;
		for(int i=0;i<planets;i++){
			double dx=level.planets[i].getX(time)-x;
			double dy=level.planets[i].getY(time)-y;
			//detect collisions
			double dist2=(dx*dx+dy*dy);
			if( (Math.sqrt(dist2)-.05) < (level.planets[i].getSize()/2.0) ){
				crashed=i;
				//Log.v("tag1","CRASH dist2 "+Math.sqrt(dist2));
				//Log.v("tag1","CRASH size "+(sizeRatio*level.sizes[i]/2.0));
			}
			
			
			double theta=Math.atan2(dy,dx);
			double mag=level.planets[i].getMass()/dist2;
			accelx+=mag*Math.cos(theta);
			accely+=mag*Math.sin(theta);
			
		}
		
		
		//apply touch
		if(useTouch && GravityView.touching){
			double dx=GravityView.touchX-x;
			double dy=GravityView.touchY-y;
			double theta=Math.atan2(dy,dx);
			double mag=.2;
			accelx+=mag*Math.cos(theta);
			accely+=mag*Math.sin(theta);
		}
		
		
		vx+=accelx*dt;
		vy+=accely*dt;
		x+=vx*dt;
		y+=vy*dt;
		
		return crashed;
		
	}
	
	public void setvx(double vx1){
		vx=vx1;
	}
	public void setvy(double vy1){
		vy=vy1;
	}
}
