package com.example.gravity;

import android.util.Log;

public class Level2{
	
	Planet[] planets;
	Rocket rocketInit;
	
	
	public Level2(int num){
		switch(num){
		case 0:
			planets=new Planet[1];
			planets[0]=new Planet(.3f, 0f, 0f);
			rocketInit=new Rocket(-.5,0,0,.7746);
	        break;
		case 1:
			planets=new Planet[2];
			planets[0]=new Planet(.3f, -.5f, 0f);
			planets[1]=new Planet(.3f, .5f, 0f);
			rocketInit=new Rocket(0,0,0,.7746);
			break;
		case 2:
			Log.v("tag1","Initializing level 3");
			planets=new Planet[2];
			planets[0]=new Planet(.3f, 0f, 0f);
			planets[1]=new Moon(.3f, 0, 0f, .5f, 5000);
			rocketInit=new Rocket(-.5,0,0,-1f);
			break;
		case 3:
			Log.v("tag1","Initializing level 3");
			planets=new Planet[2];
			planets[0]=new Moon(.3f, 0, 0f, -.5f, 5000);
			planets[1]=new Moon(.3f, 0, 0f, .5f, 5000);
			rocketInit=new Rocket(-1,0,0,.3);
			break;

		}
	}
	
	Rocket getRocket(){
		return rocketInit;
	}
	
}