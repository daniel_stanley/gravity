package com.example.gravity;

import android.content.Context;
import android.opengl.GLSurfaceView;
//import android.os.SystemClock;
//import android.util.AttributeSet;
import android.view.MotionEvent;


//
public class GravityView extends GLSurfaceView 
{	
	static float touchX;
	static float touchY;
	static boolean touching;

	//Context context;
	
        	//test comment 2
	public GravityView(Context context) 
	{
		super(context);		
	}
	
	/**
	public GravityView(Context context1, AttributeSet attrs) 
	{
		super(context1, attrs);	
		//context=context1;
	}
	**/

	@Override
	public boolean onTouchEvent(MotionEvent event) 
	{
		if (event != null)
		{			
			
			
			if (event.getAction() == MotionEvent.ACTION_DOWN)
			{
				touching=true;
				touchX = ((float)event.getX()/getWidth())*2f-1f;//range of -1 to 1
				touchY = ((float)event.getY()/getHeight())*-2f+1f;

			}else if(event.getAction() == MotionEvent.ACTION_MOVE){
				touchX = ((float)event.getX()/getWidth())*2f-1f;
				touchY = ((float)event.getY()/getHeight())*-2f+1f;
			}else if(event.getAction() == MotionEvent.ACTION_UP){
				touching=false;
			}
			
			return true;
		}
		else
		{
			return super.onTouchEvent(event);
		}		
	}

	// Hides superclass method.
	public void setRenderer(GravityRenderer renderer) 
	{
		super.setRenderer(renderer);
	}
}