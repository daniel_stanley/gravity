package com.example.gravity;

public class Level {
	
	public double[][] positions;
	public double[] sizes;
	
	
	public Level(int num){
		switch(num){
		case 0:
			double[] sizes0={.3};
	        double[][] positions0={{0,0}};
	        positions=positions0;
			sizes=sizes0;
	        break;
		case 1:
			double[] sizes1={.3,.3};
	        double[][] positions1={{-0.5,0},{0.5,0}};
	        positions=positions1;
			sizes=sizes1;
	        break;
		case 2:
			double[] sizes2={.3,.3,.3};
	        double[][] positions2={{-0.3,-.3},{.3,-.3},{0,.5}};
	        positions=positions2;
			sizes=sizes2;
	        break;
		}
		
	}
		
	
	
	public Level(double[][] positions1, double[] sizes1){
		positions=positions1;
		sizes=sizes1;
	}
	
}
